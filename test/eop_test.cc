/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 */

#include <iostream>
#include <vector>
#include "chapter2.h"
#include "chapter3.h"
#include "chapter4.h"
#include "chapter5.h"
#include "chapter6.h"

using namespace eop;
using namespace gn;

template<typename F, typename G>
auto operator >>(F f, G g)
{
  return [f, g](InputType<F, 0> a) -> Codomain<G> {
    return g(f(a));
  };
}

int sq(int x)
{
  return x * x;
}

void demo_chapter2()
{
  std::cout << std::boolalpha;
  std::cout << power_unary(2, 3, sq) << std::endl;
  auto mod5   = [](int x) { return x % 5; };
  auto sq_mod = (sq >> mod5);
  
  auto yesman = [](auto x) { return true; };
  
  std::cout << distance(3, 1, sq_mod) << std::endl;
  std::cout << square(10, std::multiplies<int>()) << std::endl;
  std::cout << square("hello|", std::plus<std::string>()) << std::endl;
  std::cout << "collision_point " << collision_point(3, sq_mod, yesman) << std::endl;
  
  auto plus3  = [](int x) { return x + 3; };
  auto pl_mod = (plus3 >> mod5);
  std::cout << terminating(3, pl_mod, yesman) << std::endl;
  std::cout << "circular_nonterminating_orbit " << circular_nonterminating_orbit(3, pl_mod) << std::endl;
  std::cout << "circular(sq_mod) " << circular(3, sq_mod, yesman) << std::endl;
  std::cout << "circular(pl_mod) " << circular(3, pl_mod, yesman) << std::endl;
  
  auto orb_s = orbit_structure(3, pl_mod, yesman);
  
  std::cout << std::get<0>(orb_s) << " "
            << std::get<1>(orb_s) << " "
            << std::get<2>(orb_s) << "\n";
}

void demo_chapter3()
{
  std::cout << power_left_associated(2, 5, std::multiplies<int>()) << std::endl;
  std::cout << power_right_associated(2, 5, std::multiplies<int>()) << std::endl;
  std::cout << power(2, 5, std::multiplies<int>(), 1) << std::endl;
  
  for (int i = 1; i < 10; i++) {
    std::cout << fibonacci(i) << ", ";
  }
  std::cout << std::endl;
}

void demo_chapter4()
{
  
  std::cout << select_0_2(3, 2, std::less<int>()) << std::endl;
  std::cout << select_1_2(4, 3, std::less<int>()) << std::endl;
  std::cout << select_0_3(3, 4, 1, std::less<int>()) << std::endl;
  std::cout << select_1_3(3, 2, 5, std::less<int>()) << std::endl;
  std::cout << select_2_3(3, 2, 5, std::less<int>()) << std::endl;
  std::cout << select_1_4(4, 3, 2, 1, std::less<int>()) << std::endl;
  std::cout << select_2_4(3, 5, 7, 1, std::less<int>()) << std::endl;
  std::cout << median_5(6, 5, 4, 3, 1, std::less<int>()) << std::endl;
}

void demo_chapter5()
{
  std::printf("rem(100, 7) = %d\n", slow_remainder(100, 7));
  std::printf("quo(100, 7) = %d\n", slow_quotient(100, 7));
  std::printf("rem(100, 7) = %d\n", remainder_nonnegative_iterative(100, 7));
  std::printf("gcd(120, 250) = %d\n", gcd(120, 250));
  
  auto quo_rem = quotient_remainder_nonnegative(100, 7);
  std::printf("rem_quo(100, 7)=(%d, %d)\n", quo_rem.first, quo_rem.second);
  quo_rem = quotient_remainder_nonnegative_iterative(100, 7);
  std::printf("rem_quo(100, 7)=(%d, %d)\n", quo_rem.first, quo_rem.second);
  
  std::printf("rem(-10, -7) = %d\n", remainder(-10, -7, remainder_nonnegative_iterative<int>));
  
  quo_rem = quotient_remainder(-10, -7, quotient_remainder_nonnegative_iterative<int>);
  std::printf("rem_quo(-10, -7)=(%d, %d)\n", quo_rem.first, quo_rem.second);
}

void demo_chapter6()
{
  std::vector<int> data = {12, 3, 4, 5, 332, 5, 46, 545, 43, 523, 54364567, 57, 8, 85};
  auto             it   = data.begin();
  increment(it);
  increment(it);
  std::cout << *it << std::endl;
  it = it - 2;
  std::cout << *it << std::endl;

//  eop::for_each(data.begin(), data.end(), [](int x){ std::cout << x << std::endl; });
  
  auto found = eop::find(data.begin(), data.end(), 8);
  std::cout << std::boolalpha << (found == data.end()) << std::endl;
  found = eop::find(data.begin(), data.end(), 22);
  std::cout << std::boolalpha << (found == data.end()) << std::endl;
  
  found = eop::find_if(data.begin(), data.end(), [](int x) { return x > 13; });
  std::cout << *found << std::endl;
  
  auto result = eop::count_if(data.begin(), data.end(), [](int x) { return x > 13; });
  std::cout << result << std::endl;
  result = std::count_if(data.begin(), data.end(), [](int x) { return x > 13; });
  std::cout << result << std::endl;
  
  auto sum = eop::reduce_nonempty(data.begin(), data.end(),
                                  std::plus<int>(), [](auto x) { return *x; });
  std::cout << sum << std::endl;
  
  sum = eop::reduce(data.begin(),
                    data.end(),
                    std::plus<int>(),
                    [](auto x) { return *x; },
                    0);
  std::cout << sum << std::endl;
  
  sum = eop::reduce_nonzeroes(data.begin(),
                              data.end(),
                              std::plus<int>(),
                              [](auto x) { return *x; },
                              0);
  std::cout << sum << std::endl;
  
  eop::for_each_n(data.begin(),
                  3,
                  [](auto x) { std::cout << x << std::endl; });
  
  auto i_n_pair = eop::find_n(data.begin(), 10, 4);
  std::cout << std::distance(data.begin(), i_n_pair.first) << std::endl;
  std::cout << i_n_pair.second << std::endl;
  
  std::cout << *eop::find_if_unguarded(data.begin(),
                                       [](auto x) -> bool { return x > 13; })
            << std::endl;
  
  std::vector<int> data2 = {12, 3, 3, 5, 332, 5, 46, 545, 43, 523, 54364567, 57, 8, 85};
  
  auto i_i_pair = eop::find_mismatch(data.begin(),
                                     data.end(),
                                     data2.begin(),
                                     data2.end(),
                                     std::equal_to<int>());
  std::cout << std::distance(data.begin(), i_i_pair.first) << std::endl;
  
  std::vector<int> data3 = {1, 1, 1, 1,  3, 4, 4};
  std::cout << std::distance(data3.begin(),
                             eop::find_adjacent_mismatch(data3.begin(),
                                                         data3.end(),
                                                         std::equal_to<int>()))
            << std::endl;
  
  auto p = [](auto x) { return x > 2; };
  std::cout << eop::partitioned(data3.begin(),
                                data3.end(), p)
            << std::endl;
  
  std::cout << std::distance(data3.begin(),
                             eop::partition_point_n(data3.begin(),
                                                    data3.size(),
                                                    p))
            << std::endl;
  std::cout << std::distance(data3.begin(),
                             eop::partition_point(data3.begin(),
                                                  data3.end(),
                                                  p))
            << std::endl;
  
  std::cout << std::distance(data3.begin(),
                             eop::lower_bound_n(
                                 data3.begin(),
                                 data3.size(),
                                 3,
                                 std::less<int>()
                             ))
            << std::endl;
  
  std::cout << std::distance(data3.begin(),
                             eop::upper_bound_n(
                                 data3.begin(),
                                 data3.size(),
                                 3,
                                 std::less<int>()
                             ))
            << std::endl;
}


int main()
{
  demo_chapter2();
  std::cout << "------------------------------\n";
  demo_chapter3();
  std::cout << "------------------------------\n";
  demo_chapter4();
  std::cout << "------------------------------\n";
  demo_chapter5();
  std::cout << "------------------------------\n";
  demo_chapter6();
  
  return 0;
}

