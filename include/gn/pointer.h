/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 */

#pragma once

#include "type_functions.h"

namespace gn
{

template<typename T>
inline
bool is_null(pointer<T> ptr) { return ptr == nullptr; }

//template<typename T>
//inline
//T& source(pointer<T> ptr) { return *ptr; }

template<typename T>
inline
pointer<T> addressof(T& x)
{
  return &x;
}

template<typename T>
inline
pointer<const T> addressof(const T& x)
{
  return &x;
}
  
}