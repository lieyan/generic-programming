/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 * Origin: Stepanov's book - Elements of Programming
 */

#pragma once

#include <cmath>

namespace eop
{
template<typename I>
bool even(I n)
{
  return n % I(2) == I(0);
}

template<typename I>
bool odd(I n)
{
  return not even(n);
}

template<typename I>
bool one(I n)
{
  return n == I(1);
}

template<typename I>
bool zero(I n)
{
  return n == I(0);
}

template<typename I>
I half_nonnegative(I n)
{
  return n / I(2);
}

template<typename I>
I half(I n)
{
  return n / I(2);
}

template<typename I>
I twice(I n)
{
  return n * I(2);
}

template<typename I>
I successor(I n)
{
  return n + 1;
}

template <typename I>
I predecessor(I n)
{
  return n - 1;
}
  
}