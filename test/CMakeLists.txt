cmake_minimum_required(VERSION 3.6)
project(genpro)

file(GLOB_RECURSE HEADERS "*.h")


add_executable(eop_test eop_test.cc ${HEADERS})