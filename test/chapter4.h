/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 */

#pragma once

#include <gn/type_functions.h>
#include <ciso646>

namespace eop
{

template<typename R>
//requires(Relation(R))
const Domain <R>& select_0_2(const Domain <R>& a,
                             const Domain <R>& b, R r)
{
  // Precondition: weak_ordering(r)
  if (r(b, a)) return b;
  return a;
}

template<typename R>
//requires(Relation(R))
const Domain <R>& select_1_2(const Domain <R>& a,
                             const Domain <R>& b, R r)
{
  // Precondition: weak_ordering(r)
  if (r(b, a)) return a;
  return b;
}

template<typename R>
//requires(Relation(R))
const Domain <R>& select_0_3(const Domain <R>& a,
                             const Domain <R>& b,
                             const Domain <R>& c, R r)
{
  return select_0_2(select_0_2(a, b, r), c, r);
}

template<typename R>
//requires(Relation(R))
const Domain <R>& select_2_3(const Domain <R>& a,
                             const Domain <R>& b,
                             const Domain <R>& c, R r)
{
  return select_1_2(select_1_2(a, b, r), c, r);
}

template<typename R>
//requires(Relation(R))
const Domain <R>& select_1_3_ab(const Domain <R>& a,
                                const Domain <R>& b,
                                const Domain <R>& c, R r)
{
  if (not r(c, b)) return b;
  return select_1_2(a, c, r);
}

template<typename R>
//requires(Relation(R))
const Domain <R>& select_1_3(const Domain <R>& a,
                             const Domain <R>& b,
                             const Domain <R>& c, R r)
{
  if (r(b, a)) return select_1_3_ab(b, a, c, r);
  return select_1_3_ab(a, b, c, r);
}

template<typename R>
//requires(Relation(R))
const Domain <R>& select_1_4_ab_cd(const Domain <R>& a,
                                   const Domain <R>& b,
                                   const Domain <R>& c,
                                   const Domain <R>& d, R r)
{
  if (r(c, a)) return select_0_2(a, d, r);
  return select_0_2(b, c, r);
}


template<typename R>
//requires(Relation(R))
const Domain <R>& select_1_4_ab(const Domain <R>& a,
                                const Domain <R>& b,
                                const Domain <R>& c,
                                const Domain <R>& d, R r)
{
  if (r(d, c)) return select_1_4_ab_cd(a, b, d, c, r);
  return select_1_4_ab_cd(a, b, c, d, r);
}


template<typename R>
//requires(Relation(R))
const Domain <R>& select_1_4(const Domain <R>& a,
                             const Domain <R>& b,
                             const Domain <R>& c,
                             const Domain <R>& d, R r)
{
  if (r(b, a)) return select_1_4_ab(b, a, d, c, r);
  return select_1_4_ab(a, b, c, d, r);
}


template<typename R>
//requires(Relation(R))
const Domain <R>& select_2_4_ab_cd(const Domain <R>& a,
                                   const Domain <R>& b,
                                   const Domain <R>& c,
                                   const Domain <R>& d, R r)
{
  if (r(d, b)) return select_1_2(a, d, r);
  return select_1_2(b, c, r);
}

template<typename R>
//requires(Relation(R))
const Domain <R>& select_2_4_ab(const Domain <R>& a,
                                const Domain <R>& b,
                                const Domain <R>& c,
                                const Domain <R>& d, R r)
{
  if (r(d, c)) return select_2_4_ab_cd(a, b, d, c, r);
  return select_2_4_ab_cd(a, b, c, d, r);
}


template<typename R>
//requires(Relation(R))
const Domain <R>& select_2_4(const Domain <R>& a,
                             const Domain <R>& b,
                             const Domain <R>& c,
                             const Domain <R>& d, R r)
{
  if (r(b, a)) return select_2_4_ab(b, a, c, d, r);
  return select_2_4_ab(a, b, c, d, r);
}


template<bool strict, typename R>
//requires(Relation(R))
struct compare_strict_or_reflexive;

template<int ia, int ib, typename R>
//requires(Relation(R))
const Domain <R>& select_0_2(const Domain <R>& a,
                             const Domain <R>& b, R r)
{
  compare_strict_or_reflexive<(ia < ib), R> cmp;
  if (cmp(b, a, r)) return b;
  return a;
}

template<typename R>
//requires(Relation(R))
struct compare_strict_or_reflexive<true, R> // strict
{
  bool operator ()(const Domain <R>& a,
                   const Domain <R>& b, R r)
  {
    return r(a, b);
  }
};

template<typename R>
//requires(Relation(R))
struct compare_strict_or_reflexive<false, R> // reflexive
{
  bool operator ()(const Domain <R>& a,
                   const Domain <R>& b, R r)
  {
    return not r(b, a);
  }
};

template<int ia, int ib, int ic, int id, typename R>
//requires(Relation(R))
const Domain <R>& select_1_4_ab_cd(const Domain <R>& a,
                                   const Domain <R>& b,
                                   const Domain <R>& c,
                                   const Domain <R>& d, R r)
{
  compare_strict_or_reflexive<(ia < ic), R> cmp;
  if (cmp(c, a, r)) return select_0_2<ia, id>(a, d, r);
  return select_0_2<ib, ic>(b, c, r);
}


template<int ia, int ib, int ic, int id, typename R>
//requires(Relation(R))
const Domain <R>& select_1_4_ab(const Domain <R>& a,
                                const Domain <R>& b,
                                const Domain <R>& c,
                                const Domain <R>& d, R r)
{
  compare_strict_or_reflexive<(ic < id), R> cmp;
  if (cmp(d, c, r)) return select_1_4_ab_cd<ia, ib, id, ic>(a, b, d, c, r);
  return select_1_4_ab_cd<ia, ib, ic, id>(a, b, c, d, r);
}

template<int ia, int ib, int ic, int id, typename R>
//requires(Relation(R))
const Domain <R>& select_1_4(const Domain <R>& a,
                             const Domain <R>& b,
                             const Domain <R>& c,
                             const Domain <R>& d, R r)
{
  compare_strict_or_reflexive<(ia < ib), R> cmp;
  if (cmp(b, a, r)) return select_1_4_ab<ib, ia, ic, id>(b, a, c, d, r);
  return select_1_4_ab<ia, ib, ic, id>(a, b, c, d, r);
}

template<int ia, int ib, int ic, int id, int ie, typename R>
//requires(Relation(R))
const Domain <R>& select_2_5_ab_cd(const Domain <R>& a,
                                   const Domain <R>& b,
                                   const Domain <R>& c,
                                   const Domain <R>& d,
                                   const Domain <R>& e, R r)
{
  compare_strict_or_reflexive<(ia < ic), R> cmp;
  if (cmp(c, a, r)) {
    return select_1_4_ab<ia, ib, id, ie>(a, b, d, e, r);
  }
  return select_1_4_ab<ic, id, ib, ie>(c, d, b, e, r);
}

template<int ia, int ib, int ic, int id, int ie, typename R>
//requires(Relation(R))
const Domain <R>& select_2_5_ab(const Domain <R>& a,
                                const Domain <R>& b,
                                const Domain <R>& c,
                                const Domain <R>& d,
                                const Domain <R>& e, R r)
{
  compare_strict_or_reflexive<(ic < id), R> cmp;
  if (cmp(d, c, r)) {
    return select_2_5_ab_cd<ia, ib, id, ic, ie>(a, b, d, c, e, r);
  }
  return select_2_5_ab_cd<ia, ib, ic, id, ie>(a, b, c, d, e, r);
}

template<int ia, int ib, int ic, int id, int ie, typename R>
//requires(Relation(R))
const Domain <R>& select_2_5(const Domain <R>& a,
                             const Domain <R>& b,
                             const Domain <R>& c,
                             const Domain <R>& d,
                             const Domain <R>& e, R r)
{
  compare_strict_or_reflexive<(ia < ib), R> cmp;
  if (cmp(b, a, r)) {
    return select_2_5_ab<ib, ia, ic, id, ie>(b, a, c, d, e, r);
  }
  return select_2_5_ab<ia, ib, ic, id, ie>(a, b, c, d, e, r);
}

template<typename R>
//requires(Relation(R))
const Domain <R>& median_5(const Domain <R>& a,
                           const Domain <R>& b,
                           const Domain <R>& c,
                           const Domain <R>& d,
                           const Domain <R>& e, R r)
{
  return select_2_5<0, 1, 2, 3, 4>(a, b, c, d, e, r);
}


/*** mutable versions below *********************************************/

template<typename R>
//requires(Relation(R))
Domain <R>& select_0_2(Domain <R>& a,
                       Domain <R>& b, R r)
{
  // Precondition: weak_ordering(r)
  if (r(b, a)) return b;
  return a;
}

template<typename R>
//requires(Relation(R))
Domain <R>& select_1_2(Domain <R>& a,
                       Domain <R>& b, R r)
{
  // Precondition: weak_ordering(r)
  if (r(b, a)) return a;
  return b;
}

template<typename R>
//requires(Relation(R))
Domain <R>& select_0_3(Domain <R>& a,
                       Domain <R>& b,
                       Domain <R>& c, R r)
{
  return select_0_2(select_0_2(a, b, r), c, r);
}

template<typename R>
//requires(Relation(R))
Domain <R>& select_2_3(Domain <R>& a,
                       Domain <R>& b,
                       Domain <R>& c, R r)
{
  return select_1_2(select_1_2(a, b, r), c, r);
}

template<typename R>
//requires(Relation(R))
Domain <R>& select_1_3_ab(Domain <R>& a,
                          Domain <R>& b,
                          Domain <R>& c, R r)
{
  if (!r(c, b)) return b;
  return select_1_2(a, c, r);
}

template<typename R>
//requires(Relation(R))
Domain <R>& select_1_3(Domain <R>& a,
                       Domain <R>& b,
                       Domain <R>& c, R r)
{
  if (r(b, a)) return select_1_3_ab(b, a, c, r);
  return select_1_3_ab(a, b, c, r);
}

template<typename R>
//requires(Relation(R))
Domain <R>& select_1_4_ab_cd(Domain <R>& a,
                             Domain <R>& b,
                             Domain <R>& c,
                             Domain <R>& d, R r)
{
  if (r(c, a)) return select_0_2(a, d, r);
  return select_0_2(b, c, r);
}


template<typename R>
//requires(Relation(R))
Domain <R>& select_1_4_ab(Domain <R>& a,
                          Domain <R>& b,
                          Domain <R>& c,
                          Domain <R>& d, R r)
{
  if (r(d, c)) return select_1_4_ab_cd(a, b, d, c, r);
  return select_1_4_ab_cd(a, b, c, d, r);
}


template<typename R>
//requires(Relation(R))
Domain <R>& select_1_4(Domain <R>& a,
                       Domain <R>& b,
                       Domain <R>& c,
                       Domain <R>& d, R r)
{
  if (r(b, a)) return select_1_4_ab(b, a, d, c, r);
  return select_1_4_ab(a, b, c, d, r);
}


template<typename R>
//requires(Relation(R))
Domain <R>& select_2_4_ab_cd(Domain <R>& a,
                             Domain <R>& b,
                             Domain <R>& c,
                             Domain <R>& d, R r)
{
  if (r(d, b)) return select_1_2(a, d, r);
  return select_1_2(b, c, r);
}

template<typename R>
//requires(Relation(R))
Domain <R>& select_2_4_ab(Domain <R>& a,
                          Domain <R>& b,
                          Domain <R>& c,
                          Domain <R>& d, R r)
{
  if (r(d, c)) return select_2_4_ab_cd(a, b, d, c, r);
  return select_2_4_ab_cd(a, b, c, d, r);
}


template<typename R>
//requires(Relation(R))
Domain <R>& select_2_4(Domain <R>& a,
                       Domain <R>& b,
                       Domain <R>& c,
                       Domain <R>& d, R r)
{
  if (r(b, a)) return select_2_4_ab(b, a, c, d, r);
  return select_2_4_ab(a, b, c, d, r);
}

template<int ia, int ib, typename R>
//requires(Relation(R))
Domain <R>& select_0_2(Domain <R>& a,
                       Domain <R>& b, R r)
{
  compare_strict_or_reflexive<(ia < ib), R> cmp;
  if (cmp(b, a, r)) return b;
  return a;
}

template<int ia, int ib, int ic, int id, typename R>
//requires(Relation(R))
Domain <R>& select_1_4_ab_cd(Domain <R>& a,
                             Domain <R>& b,
                             Domain <R>& c,
                             Domain <R>& d, R r)
{
  compare_strict_or_reflexive<(ia < ic), R> cmp;
  if (cmp(c, a, r)) return select_0_2<ia, id>(a, d, r);
  return select_0_2<ib, ic>(b, c, r);
}


template<int ia, int ib, int ic, int id, typename R>
//requires(Relation(R))
Domain <R>& select_1_4_ab(Domain <R>& a,
                          Domain <R>& b,
                          Domain <R>& c,
                          Domain <R>& d, R r)
{
  compare_strict_or_reflexive<(ic < id), R> cmp;
  if (cmp(d, c, r)) return select_1_4_ab_cd<ia, ib, id, ic>(a, b, d, c, r);
  return select_1_4_ab_cd<ia, ib, ic, id>(a, b, c, d, r);
}

template<int ia, int ib, int ic, int id, typename R>
//requires(Relation(R))
Domain <R>& select_1_4(Domain <R>& a,
                       Domain <R>& b,
                       Domain <R>& c,
                       Domain <R>& d, R r)
{
  compare_strict_or_reflexive<(ia < ib), R> cmp;
  if (cmp(b, a, r)) return select_1_4_ab<ib, ia, ic, id>(b, a, c, d, r);
  return select_1_4_ab<ia, ib, ic, id>(a, b, c, d, r);
}

template<int ia, int ib, int ic, int id, int ie, typename R>
//requires(Relation(R))
Domain <R>& select_2_5_ab_cd(Domain <R>& a,
                             Domain <R>& b,
                             Domain <R>& c,
                             Domain <R>& d,
                             Domain <R>& e, R r)
{
  compare_strict_or_reflexive<(ia < ic), R> cmp;
  if (cmp(c, a, r)) {
    return select_1_4_ab<ia, ib, id, ie>(a, b, d, e, r);
  }
  return select_1_4_ab<ic, id, ib, ie>(c, d, b, e, r);
}

template<int ia, int ib, int ic, int id, int ie, typename R>
//requires(Relation(R))
Domain <R>& select_2_5_ab(Domain <R>& a,
                          Domain <R>& b,
                          Domain <R>& c,
                          Domain <R>& d,
                          Domain <R>& e, R r)
{
  compare_strict_or_reflexive<(ic < id), R> cmp;
  if (cmp(d, c, r)) {
    return select_2_5_ab_cd<ia, ib, id, ic, ie>(a, b, d, c, e, r);
  }
  return select_2_5_ab_cd<ia, ib, ic, id, ie>(a, b, c, d, e, r);
}

template<int ia, int ib, int ic, int id, int ie, typename R>
//requires(Relation(R))
Domain <R>& select_2_5(Domain <R>& a,
                       Domain <R>& b,
                       Domain <R>& c,
                       Domain <R>& d,
                       Domain <R>& e, R r)
{
  compare_strict_or_reflexive<(ia < ib), R> cmp;
  if (cmp(b, a, r)) {
    return select_2_5_ab<ib, ia, ic, id, ie>(b, a, c, d, e, r);
  }
  return select_2_5_ab<ia, ib, ic, id, ie>(a, b, c, d, e, r);
}

template<typename R>
//requires(Relation(R))
Domain <R>& median_5(Domain <R>& a,
                     Domain <R>& b,
                     Domain <R>& c,
                     Domain <R>& d,
                     Domain <R>& e, R r)
{
  return select_2_5<0, 1, 2, 3, 4>(a, b, c, d, e, r);
}
  
  
}