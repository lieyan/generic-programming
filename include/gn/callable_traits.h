/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 */

#pragma once

#include <tuple>
#include <type_traits>

namespace gn
{

template<typename C>
struct callable_traits // matches functor or lambda
{
  using plain_C     = std::decay_t<C>;
  using func_type   = decltype(&plain_C::operator ());
  using result_type = typename callable_traits<std::decay_t<func_type>>::result_type;
  using args_type   = typename callable_traits<std::decay_t<func_type>>::args_type;
};

template<typename R, typename C, typename ... Args>
struct callable_traits<R(C::*)(Args...)>
{
  using result_type = R;
  using args_type   = std::tuple<Args...>;
};

template<typename R, typename C, typename ... Args>
struct callable_traits<R(C::*)(Args...) const> // matches lambda
{
  using result_type = R;
  using args_type   = std::tuple<Args...>;
};

template<typename R, typename ... Args>
struct callable_traits<R(*)(Args...)>
{
  using result_type = R;
  using args_type   = std::tuple<Args...>;
};

template<typename R, typename ... Args>
struct callable_traits<R(&)(Args...)>
{
  using result_type = R;
  using args_type   = std::tuple<Args...>;
};

template<typename F, int i>
using RawInputType = typename std::tuple_element<i, typename gn::callable_traits<F>::args_type>::type;

template <typename F, int i>
using InputType = std::decay_t<RawInputType<F,i>>;

template<typename F>
using Domain =  InputType<F, 0>;

template<typename F>
using Codomain = typename callable_traits<F>::result_type;
  
}